
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>DNS Lookup - HowtoseeIp</title>
<link rel="icon" href="images/favicon.ico" type="image/png">
<link rel="stylesheet" href="css/style.min.css" media="screen"/>
</head>
<body>
<header id="header" class="container clearstylefix">
<a href="/index" id="logo">
<img src="images/logo.png" width="35%" alt="SmartStart">
</a>
</header>
<section id="sectionContent" class="container clearstylefix">
<center><h1>DNS Lookup</h1></center>
<p>Our DNS Lookup tool helps you check the major DNS records of a domain name.</p><center>
<form action="dns-look-up.php" method="post" class="ip_addr_form">
<p>Domain Name: <input type="text" name="dname" ></p>
<input onclick="openClick()" type="submit" name="submit" value="Submit" class="btnSubmit">
</form>
<?php
	$domainLink = $_POST['dname'];
		$domain = file_get_contents('https://www.whoisxmlapi.com/whoisserver/DNSService?apiKey=at_MnTpkO6us316TcHDeS4qfSy7ojO1j&domainName='.$domainLink.'&type=_all');
    $ob = simplexml_load_string($domain);
    $json  = json_encode($ob);
    $response = json_decode($json, true);
?>

</center>
<br /><h4>A Records</h4>
<div style="margin-top: 15px;" id="hiddenDiv" class="row hidden">

<table class="iptable">
	<tr>
		<th>Record</th>
		<th>Hostname</th>
		<th>Class</th>
		<th>IP</th>
		<th>TTL</th>
	</tr>
		<?php foreach($response['dnsRecords']['ARecord'] as $data){?>

	<tr>
		<td>
			<center><?php echo $data['dnsType']?></center>
		</td>
		<td><?php echo $data['name']?></td>
		<td>IN</td>
		<td><?php echo $data['address']?></td>
		<td><?php echo $data['ttl']?></td>
	</tr> 
		<?php }?>

</table>
<h4>Nameservers</h4>
<table class="iptable">
	<tr>
		<th>Record</th>
		<th>Hostname</th>
		<th>Class</th>
		<th>Target</th>
		<th>TTL</th>
	</tr> 
		<?php foreach($response['dnsRecords']['NSRecord'] as $data){?>
	<tr>

		<td>
			<center><?php echo $data['dnsType']?></center>
		</td>
		<td><?php echo $data['name']?></td>
		<td>IN</td>
		<td><?php echo $data['target']?></td>
		<td><?php echo $data['ttl']?></td>
	</tr>

		<?php }?>
</table>
<h4>MX Records</h4>
<table class="iptable">
	<tr>
		<th>Record</th>
		<th>Hostname</th>
		<th>Class</th>
		<th>Target</th>
		<th>TTL</th>
	</tr> 
		<?php foreach($response['dnsRecords']['MXRecord'] as $data){?>

	<tr>
		<td>
			<center><?php echo $data['dnsType']?></center>
		</td>
		<td><?php echo $data['name']?></td>
		<td>IN</td>
		<td><?php echo $data['target']?></td>
		<td><?php echo $data['ttl']?></td>
	</tr> 
		<?php }?>

</table>
<h4>TXT Records</h4>
<table class="iptable">
	<tr>
		<th>Record</th>
		<th>Hostname</th>
		<th>Class</th>
		<th>Text</th>
		<th>TTL</th>
	</tr> 
	<?php foreach($response['dnsRecords']['TXTRecord'] as $data){?>
	<tr>
		<td>
			<center><?php echo $data['dnsType']?></center>
		</td>
		<td><?php echo $data['name']?></td>
		<td>IN</td>
		<td><?php echo $data['rawText']?></td>
		<td><?php echo $data['ttl']?></td>
	</tr> 
		<?php }?>
</table>
<h4>SOA Records</h4>
<table class="iptable">
	<tr>
		<th>Record</th>
		<th>Hostname</th>
		<th>Class</th>
		<th>MName</th>
		<th>RName</th>
		<th>Serial</th>
		<th>Refresh</th>
		<th>TTL</th>
	</tr> 
	

	<tr>
		<td>
			<center><?php echo $response['dnsRecords']['SOARecord']['dnsType']?></center>
		</td>
		<td><?php echo $response['dnsRecords']['SOARecord']['name']?></td>
		<td>IN</td>
		<td><?php echo $response['dnsRecords']['SOARecord']['host']?></td>
		<td><?php echo $response['dnsRecords']['SOARecord']['admin']?></td>
		<td><?php echo $response['dnsRecords']['SOARecord']['serial']?></td>
		<td><?php echo $response['dnsRecords']['SOARecord']['refresh']?></td>
		<td><?php echo $response['dnsRecords']['SOARecord']['ttl']?></td>
	</tr> 
		
</table>
</div>
 <br /><p>Currently, this IP lookup tool shows these DNS records: A, AAAA, CNAME, NS, MX, TXT, SOA.</p>
<hr />
</section>
<footer id="footer" class="clearstylefix">
<div class="container">
<div class="disclaimerInfo">
<p class="footerP"><b>Disclaimer:</b> The location and geo details of an IP address are not always accurate.</p>
<p class="footerP">Scrapping this website is not allowed. You will be temporarily banned if you make too many requests in an hour.</p>
</div>
</div>
</footer>
<footer id="bottom-footer" class="clearstylefix">
<div class="container">
<ul>
<li>&copy; 2000-2021 HowTOSeeIp.com</li>
<li><a href="/ip-look-up">IP Lookup</a></li>
<li><a href="/dns-look-up">DNS Lookup</a></li>
<li><a href="/contact-us" target="_blank">Contact Us</a></li>
<li><a href="/privacy-policy" target="_blank">Privacy Policy</a></li>
</ul>
</div>
</footer>
<link href="css/select.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140810102-1"></script>
<script>

function openClick() {
    document.getElementById('hiddenDiv').style.width = "100%";
    document.getElementById('hiddenDiv').style.display = "block";
    sessionStorage.setItem('clicked', true);
}
window.onload = function () {
    var data = sessionStorage.getItem('clicked');
    if (data == 'true') {
        openClick();
    }
};
</script>
</body>
</html>