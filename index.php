<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>What Is My IP Address? (IPv4 & IPv6) - How to see my Ip</title>
  <link rel="icon" href="images/favicon.ico" type="image/png">
  <link rel="stylesheet" href="css/style.min.css" media="screen" />
</head>
<body>
  <header id="header" class="container clearstylefix">
    <a href="/index" id="logo">
    <img src="images/logo.png" width="35%" alt="SmartStart">
    </a>
  </header>
<section id="sectionContent" class="container clearstylefix">
<center><h1>What is my IP?</h1></center>
<p>Check your IP address on How to see my Ip instantly with other details like the country, state and the city that IP is located in.</p>
<center><h2 id="ipHeading"></h2></center>
</div>
<b>Details:</b>
<table class="iptable">
<tr>
<td>Your IPv4</td><td><b id="tableIp"></b></td>
</tr>
<tr>
<td>Your IPv6</td><td><div id="ipv6" style="font-size: 12px;"><img src="images/loader.gif" alt="IPV6 Loader" /></div></td>
</tr>
<tr>
<td>Country</td><td id="country"></td>
</tr>
<tr>
<td>Region</td><td id="state"></td>
</tr>
<tr>
<td>City</td><td id="city"></td>
</tr>
<tr>
<td>ZIP</td><td id="zipCode"></td>
</tr>
<tr>
<td>Timezone</td><td id="timeZone"></td>
</tr>
<tr>
<td>Internet Service Provider (ISP)</td><td id="provider"></td>
</tr>
<tr>
<td>Organization</td><td id="organization"></td>
</tr>
<tr>
<td>AS number</td><td id="asNo"></td>
</tr>
<tr>
</tr>
</table>
 <div style="margin-top: 15px!important;" id="map"></div>
<h2>What is an IP Address?</h2>
<p>I bet you’ve always wondered “What’s my IP?” Well, you’re not alone in this. There are billions of computer users worldwide who also wonder “What is my IP?” too. Most of them do not have sufficient knowledge about IP addresses too. There might come a time when you may be required to perform a quick IP address lookup. It is therefore important to know the fundamental aspects of the IP address and the way they work.</p>
<p>IP is short for “Internet Protocol”. An IP address basically lets you know that you are now connected to the internet. It is one of the most basic requirements in a network and no network can be designed without the facility to provide IP addresses to track every computer and device that connects to it. The IP address connects your computer or internet-enabled device to your ISP (Internet Service Provider) which in turn connects your device to the internet.</p>
<p>In simple terms, an IP address can be defined as a computer’s virtual address in the World Wide Web. This works as a tracker for the internet to allow access to view and download information, images, emails, and other data.</p>
<p>The next time you wonder “What is my IP address?”, then think of it as a unique code that identifies your computer on the worldwide network. Your IP allows your computer to receive emails, images and other data from the web.</p>
<p>An IP address normally consists of 4 sets of 1 to 3 digits separated by a dot. The sets of numbers can range between 0 and 255 only though.</p>
<p>For example, an IP address would look something like this:</p>
<p><span style="text-decoration: underline;"><em>10.240.164.132</em></span></p>
<p>However, it will not always be the same and might change frequently. Despite this change, it will not affect the way you browse the internet. An IP address is assigned to any device that accesses the internet at any point in time.</p>
<hr style="background-color: rgba(29,161,242,1.00);" />
</section>
<footer id="footer" class="clearstylefix">
<div class="container">
<div class="disclaimerInfo ">
<p class="footerP"><b>Disclaimer:</b> The location and geo details of an IP address are not always accurate.</p>
<p class="footerP">Scrapping this website is not allowed. You will be temporarily banned if you make too many requests in an hour.</p>
</div>
</div>
</footer>
<footer id="bottom-footer" class="clearstylefix">
<div class="container">
<ul>
<li>&copy; 2020-2021 Howtoseeip.com</li>
<li><a href="/ip-look-up">IP Lookup</a></li>
<li><a href="/dns-look-up">DNS Lookup</a></li>
<li><a href="/contact-us" target="_blank">Contact Us</a></li>
<li><a href="/privacy-policy" target="_blank">Privacy Policy</a></li>
</ul>
</div>
</footer>
<link href="css/select.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>
 var request = new XMLHttpRequest();

request.open('GET', 'https://api.ipdata.co/?api-key=2d120088c59603466d810847e1f0ba580422cd24fc82ff5fd11df73e');
request.setRequestHeader('Accept', 'application/json');
request.onreadystatechange = function () {
  if (this.readyState === 4) {
    var data = this.responseText;
    var data = JSON.parse(data);
    console.log(this.responseText);
  document.getElementById("tableIp").innerHTML = data.ip;
  document.getElementById("ipHeading").innerHTML = data.ip;
  document.getElementById("country").innerHTML = data.country_name;
  document.getElementById("state").innerHTML = data.region;
  document.getElementById("city").innerHTML = data.city;
  document.getElementById("zipCode").innerHTML = data.postal;
  document.getElementById("timeZone").innerHTML = data.time_zone.name;
  document.getElementById("provider").innerHTML = data.asn.name;
  document.getElementById("organization").innerHTML = data.asn.name;
  document.getElementById("asNo").innerHTML = data.asn.asn;
  initMap(data)
  }
};

request.send();
// Initialize and add the map by function
function initMap(data) {
var latitude = data.latitude;
var longitude = data.longitude;
  // The location of Uluru
  const uluru = { lat: latitude, lng: longitude };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 6,
    center: uluru,
  });
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
  });
}
</script>
 <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4yWX7avAyqL9Ujqz27q122vDnE4u-PSM&callback=initMap&libraries=&v=weekly"
      async
    ></script>
</body>
</html>