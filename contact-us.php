
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Contact Us | Howtoseeip.com</title>
<link rel="icon" href="images/favicon.ico" type="image/png">
<link rel="stylesheet" href="css/style.min.css" media="screen" />
</head>
<body>
<header id="header" class="container clearstylefix">
<a href="/index" id="logo">
<img src="images/logo.png" width="35%" alt="SmartStart">
</a>
</header>
<section id="sectionContent" class="container clearstylefix">
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<h1>Contact Us</h1>
<p>Please contact us if you have any suggestions or feature requests related to ShowMyIP or if you want to report any error. You can also contact us if you want to advertise on this website.</p>

<div id="contact-status"></div>
<form action="" id="contactform" method="post">
<p>
<label for="name">Your Name</label>
<input type="text" name="name" class="input" value="">
</p>
<p>
<label for="email">Your Email</label>
<input type="text" name="email" class="input" value="">
</p>
<p>
<label for="message">Your Message</label>
<textarea style="max-width:90%;" name="message" cols="88" rows="6" class="textarea"></textarea>
</p>
<p><div class="g-recaptcha" data-sitekey="6LcBFMMUAAAAAKZZ5_SHMlCWer8T7-J8F4SYaaHD"></div></p>
<p>All fields are required.</p>
<input type="submit" name="submit" value="Send your message" class="button transition">
</form>
</div>

<hr />
</section>
<footer id="footer" class="clearstylefix">
<div class="container">
<div class="disclaimerInfo">
<p class="footerP"><b>Disclaimer:</b> The location and geo details of an IP address are not always accurate.</p>
<p class="footerP">Scrapping this website is not allowed. You will be temporarily banned if you make too many requests in an hour.</p>
</div>
</div>
</footer>
<footer id="bottom-footer" class="clearstylefix">
<div class="container">
<ul>
<li>&copy; 2000-2021 HowtoseeIp.com</li>
<li><a href="/ip-look-up">IP Lookup</a></li>
<li><a href="/dns-look-up">DNS Lookup</a></li>
<li><a href="/contact-us" target="_blank">Contact Us</a></li>
<li><a href="/privacy-policy" target="_blank">Privacy Policy</a></li>
</ul>
</div>
</footer>
<link href="css/select.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140810102-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '');
</script>
</body>
</html>