
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>IP Lookup - Trace the location of IP Address</title>
<link rel="icon" href="images/favicon.ico" type="image/png">
<link rel="stylesheet" href="css/style.min.css" media="screen" />

</head>
<body>
<header id="header" class="container clearstylefix">
<a href="/index" id="logo">
<img src="images/logo.png" width="35%" alt="SmartStart">
</a>
</header>
<section id="sectionContent" class="container clearstylefix">
<center><h1>IP Lookup</h1>
<p>Our IP lookup tool helps you find the country, state and the city of an IP address. It supports both IPv4 and IPv6.</p>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post"> 
	<p>IP Address: <input type="text" name="ip_addr" id="search"></p>
	<input onclick="openClick()" type="submit" name="submit" value="Submit" class="btnSubmit">
</form>
<?php
	if(count($_POST) > 0)
	{
		$ip_address=$_POST['ip_addr'];
		$location = file_get_contents('https://www.iplocate.io/api/lookup/'.$ip_address);
		$searched_Data = json_decode($location);   
	}
?>
<div style="margin-top: 15px;" id="hiddenDiv" class="row hidden">
<table class="iptable ">
<tbody><tr>
<td>Country</td><td><?php echo $searched_Data->country;?></td>
</tr>
<tr>
<td>Region</td><td><?php echo $searched_Data->subdivision;?></td>
</tr>
<tr>
<td>City</td><td><?php echo $searched_Data->city;?></td>
</tr>
<tr>
<td>ZIP</td><td><?php echo $searched_Data->postal_code;?></td>
</tr>
<tr>
<td>Timezone</td><td><?php echo $searched_Data->time_zone;?></td>
</tr>
<tr>
<td>Internet Service Provider (ISP)</td><td><?php echo $searched_Data->org;?></td>
</tr>
<tr>
<td>Organization</td><td><?php echo $searched_Data->org;?></td>
</tr>
<tr>
<td>AS number</td><td><?php echo $searched_Data->asn;?></td>
</tr>
</tbody></table>
</div>
</center>
<p><center></center></p>
<p>Supporting both IPv4 and IPv6 connection protocols, our IP lookup tool accurately collects and displays browser-specific data. Online privacy has become essential to modern-day browsing. Pixel tracking and geo-location targeting have now made it easier than ever for websites to collect visitor data.</p>
<p>Your digital fingerprint (IP address) is unique to you. With an IP lookup inquiry, you’ll have access to the information available to agencies that collect this data. IP tracker technology has advanced significantly. In conjunction with IP geolocation tracking, maintaining privacy online has become rather challenging.</p>
<p>Our tool bridges the gap between transparency and browsing by showing users what publicly-displayed information is available from their connection. Results from our site will display your IP address in addition to your postal/zip code, state/region, ISP, city/country name, and your time zone. Such data is utilized by various agencies and sites to discover who the browser of the IPv4 or IPv6 connection is.</p>
<p>By conducting an IP Lookup, you’ll see what those tracking your activity see and can take measures (if necessary) to begin protecting this data.
You may have arrived here by searching online for <a href="">what is my IP address</a>, and if so, you've come to the right place. Our IP-discovery tool serves thousands of users every month. Now, you too can benefit from our advanced IP-search tool with reporting that takes into account many factors to output the geolocation data.</p>
<hr />
</section>
<footer id="footer" class="clearstylefix">
<div class="container">
<div class="disclaimerInfo">
<p class="footerP"><b>Disclaimer:</b> The location and geo details of an IP address are not always accurate.</p>
<p class="footerP">Scrapping this website is not allowed. You will be temporarily banned if you make too many requests in an hour.</p>
</div>
</div>
</footer>
<footer id="bottom-footer" class="clearstylefix">
<div class="container">
<ul>
<li>&copy; 2000-2021 HowToSeeIp.com</li>
<li><a href="/ip-look-up">IP Lookup</a></li>
<li><a href="/dns-look-up">DNS Lookup</a></li>
<li><a href="/contact-us" target="_blank">Contact Us</a></li>
<li><a href="/privacy-policy" target="_blank">Privacy Policy</a></li>
</ul>
</div>
</footer>
<link href="css/select.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140810102-1"></script>
<script>

function openClick() {
    document.getElementById('hiddenDiv').style.width = "100%";
    document.getElementById('hiddenDiv').style.display = "block";
    sessionStorage.setItem('clicked', true);
}
window.onload = function () {
    var data = sessionStorage.getItem('clicked');
    if (data == 'true') {
        openClick();
    }
};
</script>

</body>
</html>